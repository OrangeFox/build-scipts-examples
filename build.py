# OrangeFox Recovery Builder by MrYacha for Red Server

import os
import subprocess
import argparse
import random
import string
import time
from termcolor import colored
import re

parser = argparse.ArgumentParser(description='Build the OrangeFox.')
parser.add_argument('devices', metavar='devices', type=str, nargs='+',  help='Devices to build')
parser.add_argument('--clean', '-c', action="store_true", help="Make a clean build")
parser.add_argument("-t", "--type", type=str, choices=['S', 'B', 'C', 'A', "P", "R"],
                    default="C", help="Build type")
parser.add_argument("-v", "--ver", type=str, default="10.0", help="Major version")
parser.add_argument("--tver", type=str, default="", help="Minor version")

args = parser.parse_args()

# == Constatns ==
need_clean = 1


# == Functions ==

def random_char(y):
    nums = "1234567890"
    return ''.join(random.choice(string.ascii_letters + nums + nums) for x in range(y))


def bash_cmd(cmd):
    process = subprocess.Popen(
        cmd,
        shell=True,
        executable='/bin/bash',
        env=dict(os.environ),
        stdout=subprocess.PIPE
    )
    text = ""
    while True:
        output = process.stdout.readline()
        text += output.strip().decode() + '\n'
        if not output or output == '' and process.poll() is not None:
            return text
        if output:
            print(output.strip().decode())
    return text


os.environ["LC_ALL"] = "C"
os.environ["ALLOW_MISSING_DEPENDENCIES"] = "true"
os.environ["USE_CCACHE"] = "true"
os.environ["CCACHE_DIR"] = "/ramdisk/ccache"

# == Constants ==
CCACHE_CMD = """ccache -M 190G;"""

BUILD_TYPES = {
    "S": "Stable",
    "B": "Beta",
    "R": "RC",
    "C": "Confidential",
    "P": "PreAlpha_" + args.tver,
    "A": "Alpha_" + args.tver
}

VERSION = "R" + str(args.ver)

if args.tver:
    VERSION += "." + str(args.tver)

# Add generated symols if Build Type isn't a Stable
if not args.type == "S":
    if not args.tver:
        VERSION += "_" + random_char(3)
    os.environ["FOX_ENABLE_LAB"] = "1"

# == Devices ==

# Example:
#    'device_codename': 'build_env'
#
# Avaible build envs:
# - PIE: Official OrangeFox Omni Android 9.0 Pie Build env
# - OREO: Official OrangeFox Omni Android 8.1 Oreo Build env
# - NOUGAT: Official OrangeFox Omni Android 7.1 Nougat Build env

DEVICES = {
    'beryllium': 'PIE',
    'Z00ED': 'NOUGAT'
}

DEVICES_WITH_FLASHLIGHT = ()
NON_MAGISK_BOOT_DEVICES = ()
NON_MIUI_DEVICES = ()
SCREEN_18_9_DEVICES = ()

# ==

print("=== OrangeFox Recovery ===")
print(colored("Build Type: ", 'yellow') + BUILD_TYPES[args.type])
print(colored("Version: ", 'yellow') + VERSION)
print("=== OrangeFox Recovery ===")

if 'all' in args.devices:
    print(colored("\n* Start building for all devices...", 'cyan', attrs=['bold']))
    devices = DEVICES

else:
    devices = args.devices

for device in devices:
    print(colored("\n* Start building for " + device, 'cyan', attrs=['bold']))

    # == Default vars ==
    base_rom = 'omni'
    os.environ['FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER'] = "1"
    # ==

    build_env = DEVICES[device]
    if build_env == "PIE":
        print(colored("- Build system: ", 'yellow') + "Pie")
        s_cmd = "cd OrangeFox-Pie;"
    elif build_env == "OREO":
        print(colored("- Build system: ", 'yellow') + "Oreo")
        s_cmd = "cd OrangeFox-Oreo;"
    elif build_env == "NOUGAT":
        print(colored("- Build system: ", 'yellow') + "Nougat (Legacy)")
        s_cmd = "cd OrangeFox-Nougat;"
        os.environ['FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER'] = "0" # Nougat not compability

    s_cmd += "source build/envsetup.sh;" + CCACHE_CMD

    # Update sources
    s_cmd += "rm -rf bootable/recovery;"
    s_cmd += "cp -r ~/Sources/OrangeFox-Recovery/ bootable/recovery;"

    s_cmd += "rm -rf vendor/recovery;"
    s_cmd += "cp -r ~/Sources/OrangeFox-Vendor/ vendor/recovery;"


    if args.clean and need_clean == 1:
        print(colored("- Clean build: ", 'yellow') + "Yes")
        bash_cmd(s_cmd + 'make clean')
        need_clean = 0

    # == Restore default vars ==
    os.environ["TW_DEVICE_VERSION"] = VERSION
    os.environ["BUILD_TYPE"] = BUILD_TYPES[args.type]

    os.environ["OF_MAINTAINER"] = "1"
    os.environ["OF_SCREEN_H"] = "1920"
    os.environ["OF_NO_TREBLE_COMPATIBILITY_CHECK"] = '0'
    os.environ["FOX_BUILD_FULL_KERNEL_SOURCES"] = '1'

    #
    os.environ["OF_USE_TWFUNC_REBOOT_FUNCTION"] = "1"
    #os.environ["FOX_RESET_SETTINGS"] = "1"
    #os.environ["FOX_USE_BASH_SHELL"] = "0" 
    os.environ["OF_SUPPORT_PRE_FLASH_SCRIPT"] = "1" 
    # ==

    if device in DEVICES_WITH_FLASHLIGHT:
        print(colored("- Flashlight: ", 'yellow') + "Yes")
        os.environ["OF_FLASHLIGHT_ENABLE"] = "1"
    else:
        print(colored("- Flashlight: ", 'yellow') + "No")
        os.environ["OF_FLASHLIGHT_ENABLE"] = "0"

    if device in NON_MAGISK_BOOT_DEVICES:
        print(colored("- Magiskboot: ", 'yellow') + "No")
        os.environ["OF_USE_MAGISKBOOT"] = "0"
    else:
        print(colored("- Magiskboot: ", 'yellow') + "Yes")
        os.environ["OF_USE_MAGISKBOOT"] = "1"

    if device in NON_MIUI_DEVICES:
        print(colored("- MIUI Features: ", 'yellow') + "No")
        os.environ["OF_DISABLE_MIUI_SPECIFIC_FEATURES"] = "1"
    else:
        print(colored("- MIUI Features: ", 'yellow') + "Yes")
        os.environ["OF_DISABLE_MIUI_SPECIFIC_FEATURES"] = "0"

    if device in SCREEN_18_9_DEVICES:
        print(colored("- Device have 18.9 screen", 'yellow'))
        os.environ["OF_SCREEN_H"] = "2160"

    print(colored("\n==== Building... ====\n", 'cyan', attrs=['bold']))

    txt = bash_cmd(s_cmd + f"lunch {base_rom}_{device}-eng && mka recoveryimage")
    print(colored("\n- Build finished.", 'cyan', attrs=['bold']))

    # h = re.search(r'Recovery zip: (.+)', txt)
    # zip = h.group(1)

print(colored("\n- All builds are finished.", 'cyan', attrs=['bold']))
print("=== OrangeFox Recovery ===")
